using System.Collections.Generic;
using UnityEngine;
using AYellowpaper.SerializedCollections;
using Unity.VisualScripting.Antlr3.Runtime.Misc;

[CreateAssetMenu(menuName = "Upgrade Stats")]
public class Upgrade : ScriptableObject
{
    public string upgradeName;
    public SerializedDictionary<Stat, Sprite> icons;
    public SerializedDictionary<Stat, int> upgradeCost;
    public string upgradeDescription { get; private set; }

    public void IncreaseCost(Stat stat)
    {
        if(upgradeCost.TryGetValue(stat, out int value))
        {
            upgradeCost[stat] *= 2;
        }
    }

    public int GetStat(Stat stat)
    {
        if (upgradeCost.TryGetValue(stat, out int value))
        {
            return value;
        }
        else
        {
            Debug.LogError($"No stats found {stat} on {this.name}");
            return 0;
        }
    }

    public int SetStat(Stat stat, int amount)
    {
        if (upgradeCost.TryGetValue(stat, out int value))
        {
            upgradeCost[stat] = amount;
            return upgradeCost[stat];
        }
        else
        {
            return 1;
        }
    }
}
