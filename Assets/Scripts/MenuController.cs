using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{

    [Header("UI Elements")]
    [SerializeField] private Button startButton;
    [SerializeField] private Button quitButton;
    [SerializeField] private Button deleteSaveButton;


    void Start()
    {
        startButton.onClick.AddListener(() => OnStart());
        quitButton.onClick.AddListener(() => Application.Quit());
        deleteSaveButton.onClick.AddListener(() => DeleteSave());
    }

    private void OnStart()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
    }

    private void DeleteSave()
    {
        SaveSystem.DeleteSave();
    }
}
