using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public Stats playerStats;
    private Enemy enemy;
    public int coin;

    [SerializeField] private Text coinText;

    [SerializeField] private Button ability;
    [SerializeField] private ParticleSystem moneyParticle;
    [SerializeField] private ParticleSystem clickParticle;
    public float currentTime { get; private set; }
    public float autoAttackTime;

    public Action OnCoinIncreased;

    public bool AbilityReady { get; private set; }

    private void Awake()
    {
        AbilityReady = false;
        LoadPlayerData();
    }

    private void Start()
    {
        OnCoinIncreased?.Invoke();
        coinText.text = $"{coin}";
        ability.onClick.AddListener(() => UseAbility());
    }

    private void FixedUpdate()
    {
        if (currentTime <= playerStats.GetStat(Stat.reloadAbility) && !AbilityReady)
        {
            currentTime += Time.fixedDeltaTime;
        }
        else if(!AbilityReady && currentTime >= playerStats.GetStat(Stat.reloadAbility))
        {

            AbilityReady = true;
            currentTime = playerStats.GetStat(Stat.reloadAbility);
        }
        if(!AbilityReady)
        {
            ability.GetComponent<Image>().fillAmount = Mathf.InverseLerp(0, playerStats.GetStat(Stat.reloadAbility), currentTime);
            ability.interactable = false;
        }
        if(AbilityReady)
        {
            ability.interactable = true;
            ability.GetComponent<Image>().fillAmount = 1;
        }
    }

    private void Update()
    {
        if(enemy != null)
        {
            if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                if (!enemy.IsDead)
                {
                    enemy.DecreaseHealth((int)playerStats.GetStat(Stat.attack));
                    Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    clickParticle.Play();
                    clickParticle.transform.position = new Vector3(mousePos.x, mousePos.y, 0);
                }
            }
            AutoAttack();
        }
        else
        {
            enemy = FindObjectOfType<Enemy>();
        }

    }

    private void AutoAttack()
    {
        if (autoAttackTime <= 0)
        {
            autoAttackTime = 1;
            if (!enemy.IsDead)
            {
                enemy.DecreaseHealth((int)playerStats.GetStat(Stat.autoAttack));
            }
        }
        else
        {
            autoAttackTime -= Time.deltaTime;
        }
    }

    private void UseAbility()
    {
        if(AbilityReady)
        {
            if(!enemy.IsDead)
            {
                enemy.DecreaseHealth((int)playerStats.GetStat(Stat.attack) * 10);
                moneyParticle.Play();
                SoundController.Instance.PlaySound(SoundController.Sounds.ability);
                AbilityReady = false;
                currentTime = 0;
            }
        }
    }

    public void IncreaseCoin()
    {
        coin += (int)(20 * playerStats.GetStat(Stat.moneyModifier));
        OnCoinIncreased?.Invoke();
        coinText.text = $"{coin}";
    }

    public void UpdateCoin()
    {
        coinText.text = $"{coin}";
    }

    private void OnApplicationQuit()
    {
        SaveSystem.SaveGameData(this,FindObjectOfType<Enemy>(),FindObjectOfType<PlayerUpgrades>());
    }

    private void LoadPlayerData()
    {
        GameData data = SaveSystem.LoadData();

        float maxColdown;

        playerStats.SetStat(Stat.attack, data.damage);
        playerStats.SetStat(Stat.reloadAbility,maxColdown = data.abilityColdown < 10 ? 10:data.abilityColdown);
        playerStats.SetStat(Stat.moneyModifier, data.moneyModifier);
        playerStats.SetStat(Stat.autoAttack, data.autoAttack);
        coin = data.coin;
        AbilityReady = data.AbilityReady;
        currentTime = data.currentTime;
    }
    private void OnEnable()
    {
        FindObjectOfType<Enemy>().OnEnemyDie += IncreaseCoin;
    }
}
