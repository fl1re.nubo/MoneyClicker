using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    [SerializeField] private Transform pausePanel;
    [SerializeField] private Transform pauseHolder;
    [SerializeField] private Button pauseButton;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private Sprite[] soundSprites;

    [SerializeField] private SpriteRenderer backgroundSprite;

    private void Awake()
    {
        Resize();
    }

    private void Start()
    {

        pauseHolder.GetChild(1).GetComponent<Button>().onClick.AddListener(() => OnClickToMenu());
        pauseHolder.GetChild(2).GetComponent<Button>().onClick.AddListener(() => OnResume());
        pauseButton.onClick.AddListener(() => OnPause());
    }

    public void OnClickToMenu()
    {
        SaveSystem.SaveGameData(FindObjectOfType<Player>(), FindObjectOfType<Enemy>(), FindObjectOfType<PlayerUpgrades>());
        SceneManager.LoadScene(0);
    }

    public void OnClickSound(Image sprite)
    {
        audioSource.mute = !audioSource.mute;
        sprite.sprite = audioSource.mute == false ? soundSprites[0] : soundSprites[1];
    }

    public void OnResume()
    {
        Time.timeScale = 1;
        pausePanel.gameObject.SetActive(false);
    }

    public void OnPause()
    {
        Time.timeScale = 0;
        pausePanel.gameObject.SetActive(true);
    }

    void Resize()
    {
        if (backgroundSprite == null) return;

        transform.localScale = new Vector3(1, 1, 1);

        float width = backgroundSprite.sprite.bounds.size.x;
        float height = backgroundSprite.sprite.bounds.size.y;


        float worldScreenHeight = Camera.main.orthographicSize * 2f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        Vector3 xWidth = backgroundSprite.transform.localScale;
        xWidth.x = worldScreenWidth / width;
        Vector3 yHeight = backgroundSprite.transform.localScale;
        yHeight.y = worldScreenHeight / height;
        backgroundSprite.transform.localScale = new Vector2(xWidth.x, yHeight.y);

    }
}
