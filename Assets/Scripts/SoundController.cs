using UnityEngine;

public class SoundController : MonoBehaviour
{
    public static SoundController Instance { get; private set; }
    public AudioSource audioSource;
    public AudioClip[] audioClips;
    public enum Sounds
    {
        ability,
        attack,
        die
    }
    public Sounds soundState;

    private void Awake()
    {
        Instance = this;
    }

    public void PlaySound(Sounds sound)
    {
        switch (sound)
        {
            case Sounds.ability:
                audioSource.PlayOneShot(audioClips[0]);
                break;
            case Sounds.attack:
                audioSource.PlayOneShot(audioClips[1]);
                break;
            case Sounds.die:
                audioSource.PlayOneShot(audioClips[2]);
                break;
        }
    }
}
