using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUpgrades : MonoBehaviour
{
    [SerializeField] Player player;
    public Upgrade playerUpgrade;


    [SerializeField] private List<Button> upgradeButtons;

    private void Awake()
    {
        LoadUpgradeData();
    }

    private void Start()
    {
        int i = 0;
        foreach (var item in playerUpgrade.icons)
        {
            upgradeButtons[i].GetComponent<Image>().sprite = item.Value;
            upgradeButtons[i].GetComponentInChildren<Text>().text = playerUpgrade.upgradeCost[item.Key].ToString();
            upgradeButtons[i].onClick.AddListener(() => UpgradeStat(item.Key, 2));
            i++;
        }

        CheckUpgrade();
    }

    public void CheckUpgrade()
    {
        int i = 0;
        foreach (var upgradeCost in playerUpgrade.upgradeCost) 
        { 
            if(upgradeCost.Value <= player.coin)
            {
                upgradeButtons[i].interactable = true;
            }
            else
            {
                upgradeButtons[i].interactable= false;
            }
            upgradeButtons[i].GetComponentInChildren<Text>().text = upgradeCost.Value.ToString();
            i++;
        }
    }

    public void UpgradeStat(Stat stat, float amount)
    {
        player.coin -= playerUpgrade.upgradeCost[stat];
        player.playerStats.ChangeStat(stat, amount);
        playerUpgrade.upgradeCost[stat] *= 2;
        CheckUpgrade();
        player.UpdateCoin();
    }

    private void OnEnable()
    {
        player.OnCoinIncreased += CheckUpgrade;
    }
    private void OnDestroy()
    {
        player.OnCoinIncreased -= CheckUpgrade;
    }

    private void LoadUpgradeData()
    {
        GameData data = SaveSystem.LoadData();

        playerUpgrade.SetStat(Stat.attack, data.costDamage);
        playerUpgrade.SetStat(Stat.reloadAbility, data.costAbilityColdown);
        playerUpgrade.SetStat(Stat.moneyModifier, data.costMoneyModifier);
        playerUpgrade.SetStat(Stat.autoAttack, data.costAutoAttack);
    }
}
