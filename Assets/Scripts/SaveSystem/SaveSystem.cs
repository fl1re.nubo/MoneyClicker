using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveSystem : MonoBehaviour
{
    public static void SaveGameData(Player player, Enemy enemy,PlayerUpgrades playerUpgrades)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/GameData.fun";
        FileStream fs = new FileStream(path, FileMode.Create);

        GameData data = new GameData(player, enemy,playerUpgrades);

        formatter.Serialize(fs, data);
        fs.Close();
    }

    public static GameData LoadData()
    {
        string path = Application.persistentDataPath + "/GameData.fun";
        if(File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = new FileStream(path, FileMode.Open);

            GameData data = formatter.Deserialize(fs) as GameData;
            fs.Close();
            return data;
        }
        else
        {
            Debug.Log("File does not exist");
            GameData data = new GameData(FindObjectOfType<Player>(),FindObjectOfType<Enemy>(),FindObjectOfType<PlayerUpgrades>());
            return data;
        }
    }

    public static void DeleteSave()
    {
        string path = Application.persistentDataPath + "/GameData.fun";
        if (File.Exists(path))
        {
            File.Delete(path);
        }
        else
        {
            Debug.Log("File does not exist");
        }
    }
}
