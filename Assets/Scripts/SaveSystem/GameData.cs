using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public int coin;
    public float damage;
    public float autoAttack;
    public float abilityColdown;
    public float moneyModifier;
    public float enemyHealth;
    public int costDamage;
    public int costAutoAttack;
    public int costAbilityColdown;
    public int costMoneyModifier;
    public float currentTime;
    public bool AbilityReady;

    public GameData(Player player, Enemy enemy,PlayerUpgrades playerUpgrades)
    {
        damage = player.playerStats.GetStat(Stat.attack);
        autoAttack = player.playerStats.GetStat(Stat.autoAttack);
        moneyModifier = player.playerStats.GetStat(Stat.moneyModifier);
        abilityColdown = player.playerStats.GetStat(Stat.reloadAbility);
        enemyHealth = enemy.enemyStats.GetStat(Stat.health);
        coin = player.coin;
        costAbilityColdown = playerUpgrades.playerUpgrade.GetStat(Stat.reloadAbility);
        costDamage = playerUpgrades.playerUpgrade.GetStat(Stat.attack);
        costAutoAttack = playerUpgrades.playerUpgrade.GetStat(Stat.autoAttack);
        costMoneyModifier = playerUpgrades.playerUpgrade.GetStat(Stat.moneyModifier);
        currentTime = player.currentTime;
        AbilityReady = player.AbilityReady;
    }
}
