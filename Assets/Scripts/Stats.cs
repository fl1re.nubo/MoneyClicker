using UnityEngine;
using AYellowpaper.SerializedCollections;

public enum Stat
{
    attack,
    health,
    reloadAbility,
    moneyModifier,
    autoAttack
}
[CreateAssetMenu(menuName = "Stats")]
public class Stats : ScriptableObject
{
    public SerializedDictionary<Stat,float> stats = new SerializedDictionary<Stat,float>();

    public float GetStat(Stat stat)
    {
        if(stats.TryGetValue(stat,out float value))
        {
            return value;
        }
        else
        {
            Debug.LogError($"No stats found {stat} on {this.name}");
            return 0f;
        }
    }

    public float SetStat(Stat stat, float amount)
    {
        if (stats.TryGetValue(stat, out float value))
        {
            stats[stat] = amount;
            return stats[stat];
        }
        else
        {
            return 1f;
        }
    }

    public float ChangeStat(Stat stat, float amount)
    {
        if(stats.TryGetValue(stat,out float value))
        {
            stats[stat] += amount;
            return stats[stat];
        }
        else
        {
            return -1f;
        }
    }
}
