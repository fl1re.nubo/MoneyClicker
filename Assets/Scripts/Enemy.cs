using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public Stats enemyStats;
    private Animator anim;

    [SerializeField]private List<GameObject> enemyPrefabs = new List<GameObject>();
    private GameObject currentEnemyPrefab;

    private Text healthText;
    private Slider healthSlider;

    private int health;
    private int maxHealth;

    public bool IsDead { get; private set; }

    public Action OnEnemyDie;

    private void Awake()
    {
        LoadEnemyData();
        SpawnEnemy();
    }

    public void DecreaseHealth(int damage)
    {
        if(health > 0)
        {
            health -= damage;

            healthSlider.value = health;
            healthText.text = $"{health}/{maxHealth}";
            SoundController.Instance.PlaySound(SoundController.Sounds.attack);
        }

        if (health <= 0)
        {
            health = 0;
            healthSlider.value = health;
            healthText.text = $"{health}/{maxHealth}";
            IsDead = true;
            StartCoroutine(OnDeathEnemy());
        }
    }

    IEnumerator OnDeathEnemy()
    {
        anim.SetTrigger("TakeDamage");
        SoundController.Instance.PlaySound(SoundController.Sounds.die);
        yield return new WaitForSeconds(0.5f);

        enemyStats.ChangeStat(Stat.health, 2);
        anim.SetTrigger("TakeDamage");
        Destroy(currentEnemyPrefab);
        OnEnemyDie?.Invoke();
        SpawnEnemy();
    }

    private void LoadEnemyData()
    {
        GameData data = SaveSystem.LoadData();

        enemyStats.SetStat(Stat.health, data.enemyHealth);
    }

    private void SpawnEnemy()
    {
        int rand = UnityEngine.Random.Range(0, enemyPrefabs.Count);
        currentEnemyPrefab = Instantiate(enemyPrefabs[rand], transform.position, Quaternion.identity);
        

        healthText = currentEnemyPrefab.transform.GetComponentInChildren<Text>();
        healthSlider = currentEnemyPrefab.transform.GetComponentInChildren<Slider>();

        SetEnemyStats();

        anim = currentEnemyPrefab.GetComponent<Animator>();
    }


    private void SetEnemyStats()
    {
        health = (int)enemyStats.GetStat(Stat.health);
        maxHealth = health;

        healthSlider.maxValue = maxHealth;
        healthSlider.value = health;

        healthText.text = $"{health}/{maxHealth}";

        IsDead = false;
    }
}
